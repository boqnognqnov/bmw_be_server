## Used libraries:
- [ backpack for laravel ](https://backpackforlaravel.com/).
- [ spatie/laravel-permission ](https://github.com/spatie/laravel-permission).
- [ Laravel-Backpack/PermissionManager ](https://github.com/Laravel-Backpack/PermissionManager).
- [ API Authentication (Passport) ](https://laravel.com/docs/5.7/passport).
- [ barryvdh/laravel-elfinder ](https://github.com/barryvdh/laravel-elfinder).

## Init new project:

- Set your environment (.env)
- Go in 'app' directory
- Run:  ```composer update```
- Run:  ```php artisan migrate:refresh --seed```
 
    Now you can access www.your-app.dev``/admin``  with email: ``super@admin.dev`` and password: ``admin``

- Run:  ```php artisan passport:install```


## Backpack create new CRUD:
- Create new migration: ``php artisan make:migration create_tags_table``
- Create a model, a request and a controller for the admin panel
  ```php artisan backpack:crud tag```  use singular, not plural
- Add a route to routes/backpack/custom.php (under the admin prefix and auth middleware): 
``php artisan backpack:base:add-custom-route "CRUD::resource('tag', 'TagCrudController');"``

- Add a sidebar item: 
  ``php artisan backpack:base:add-sidebar-content "<li><a href='{{ backpack_url('tag') }}'><i class='fa fa-tag'></i> <span>Tags</span></a></li>"``
