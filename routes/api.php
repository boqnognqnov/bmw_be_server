<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

//Route::post('login', 'API\PassportController@login');
//Route::post('register', 'API\PassportController@register');


Route::group(['middleware' => 'headers'], function () {

    Route::post('login', 'API\TestController@login');

    Route::post('policy-configurations/ilo/assemble', 'API\TestController@createIloSummary');
    Route::post('policy-configurations/portchannel/assemble', 'API\TestController@createPortChannelSummary');

    Route::get('categories', 'API\TestController@getCategories');
    Route::get('server/areas', 'API\TestController@getAreas');
    Route::get('server/configurations', 'API\TestController@getConfigurations');


    Route::get('policy-configurations/ilo', 'API\TestController@getIloProfiles');
    Route::get('policy-configurations/ilo/profiles/{profileId}/interfaces', 'API\TestController@getIloProfileInterfaces');
    Route::get('policy-configurations/ilo/types', 'API\TestController@getIloInterfaceTypes');


    Route::get('policy-configurations/portchannel', 'API\TestController@getPortChannelProfiles');
    Route::get('policy-configurations/portchannel/profiles/{profileId}/interfaces', 'API\TestController@getPortChannelProfileInterfaces');


    Route::post('server-policies', 'API\TestController@storeServerPolicies');

});




//get /policy-configurations/portchannel

//Route::group(['middleware' => 'auth:api'], function () {
//    Route::post('get-details', 'API\PassportController@getDetails');
//
//    Route::resource('categories', 'API\CategoryController');
//
////    Route::get('categories/{category_id}', 'CategoryController@show');
//
//});

