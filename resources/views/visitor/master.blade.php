@extends( 'visitor.layouts.default' )

@section( 'body' )

    @include('visitor.partials.header',['variables'=>[]])

    @yield( 'content' )

    @include('visitor.partials.footer',['variables'=>[]])

@endsection

@section( 'scripts' )


    {{--<script src="{!! asset('js/jquery.modal.min.js') !!}" type="text/javascript" charset="utf-8"></script>--}}
    {{--<script src="{!! asset('js/otherdropdown.js') !!}" type="text/javascript" charset="utf-8"></script>--}}
    {{--<script src="{!! asset('js/scripts.js') !!}" type="text/javascript" charset="utf-8"></script>--}}

@endsection
