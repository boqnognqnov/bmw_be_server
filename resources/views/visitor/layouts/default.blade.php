<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/Article">
<head>
    <meta charset="UTF-8">

    <title>@yield('page-title')</title>
    <meta name="description" content="@yield('meta-description')"/>

    <script src="{!! asset('js/jQuery.js') !!}"></script>
    <link rel="stylesheet" href="{!! asset('css/bootstrap/bootstrap.css') !!}">
    <script src="{!! asset('js/bootstrap/bootstrap.js') !!}"></script>
</head>
<body>
@yield('body')
</body>

@yield( 'scripts' )


{{--<link href="{!! asset('css/custom.css') !!}" rel='stylesheet' type='text/css'>--}}
{{--<script src="{!! asset('js/scripts.js') !!}"></script>--}}

</html>
