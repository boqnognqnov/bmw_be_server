<?php if (backpack_auth()->guest() || (!Auth::user()->hasRole('admin') && !Auth::user()->hasRole('super_admin'))) {
Auth::logout();
?>


<script type="text/javascript">
    window.location.href = "../../../../../admin/login";
</script>

<?php } ?>

@if(!Auth::guest())

    <!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
    <li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i>
            <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
    <li><a href="{{ backpack_url('elfinder') }}"><i class="fa fa-files-o"></i>
            <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
    <li><a href='{{ backpack_url('category') }}'><i class='fa fa-tag'></i> <span>Categories</span></a></li>
    <li><a href='{{ backpack_url('news') }}'><i class='fa fa-tag'></i> <span>News</span></a></li>

    <!-- Users, Roles Permissions -->
    @if (Auth::user()->can('manage_roles'))
        <li class="treeview">
            <a href="#"><i class="fa fa-group"></i> <span>Users, Roles, Permissions</span> <i
                        class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
                <li><a href="{{ backpack_url('user') }}"><i class="fa fa-user"></i> <span>Users</span></a></li>
                <li><a href="{{ backpack_url('role') }}"><i class="fa fa-group"></i> <span>Roles</span></a></li>
                <li><a href="{{ backpack_url('permission') }}"><i class="fa fa-key"></i> <span>Permissions</span></a>
                </li>
            </ul>
        </li>
    @endif

@endif



