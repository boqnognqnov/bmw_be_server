<?php

use Illuminate\Database\Seeder;

class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        dump("Create 'First super admin' user");
        dump("Email: super@admin.dev");
        dump("Password: `admin`");
        DB::table('users')->insert([
            'id' => '1',
            'name' => 'admin',
            'email' => 'super@admin.dev',
            'password' => Hash::make("admin")
        ]);

        dump("");
    }
}
