<?php

use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        dump("Create Role -> super_admin");
        $role = Role::create(['name' => 'super_admin']);
        dump("");

        dump("Create permission -> `manage_roles`");
        $permission = Permission::create(['name' => 'manage_roles']);
        dump("");

        dump("Assign permission 'manage_roles' to role `super_admin`");
        $role->givePermissionTo($permission);
        $permission->assignRole($role);
        dump("");


        dump("Assign role `super_admin` to 'First super admin' user");
        $user = \Backpack\Base\app\Models\BackpackUser::find(1);
        $user->assignRole('super_admin');
        dump("");

        dump("Create Role -> admin");
        Role::create(['name' => 'admin']);
        dump("");

        dump("Create Role -> visitor");
        Role::create(['name' => 'visitor']);
        dump("");

    }
}
