<?php
/**
 * Created by PhpStorm.
 * User: Boyan.Bozhidarov
 * Date: 3.10.2018 г.
 * Time: 13:55
 */

namespace App\Http\Controllers\Visitor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\View\View;
use TestTrait;

class HomePageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {

        return view('visitor.home_page');
    }


}