<?php
/**
 * Created by PhpStorm.
 * User: Boyan.Bozhidarov
 * Date: 3.10.2018 г.
 * Time: 13:55
 */

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;


class TestController extends Controller
{

    public function login(Request $request)
    {
        return ['token' => 'asdasdasda43242rewdasdasdasdAAAAAAAAAAAAAAA'];

//        return new \Exception("Sadasdasdasd", 401);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Category[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getCategories()
    {
        return [435345435];
    }

    public function getAreas(Request $request)
    {
        $header = $request->header('X-API-KEY');


        return [
            ['id' => 0, 'area' => $header],
            ['id' => 1, 'area' => 'Windows baremetal'],
            ['id' => 2, 'area' => 'Openstack'],
            ['id' => 3, 'area' => 'VMWare'],
            ['id' => 4, 'area' => 'Unix/Linux baremetal'],

        ];
    }


    public function getConfigurations()
    {

        return [
            ['id' => 1, 'name' => 'Portchannel + ILO22', 'portchannelCount' => 1, 'iloCount' => 1],
            ['id' => 2, 'name' => 'ILO only', 'portchannelCount' => 0, 'iloCount' => 1],
            ['id' => 3, 'name' => 'Portchannel only', 'portchannelCount' => 1, 'iloCount' => 0],
            ['id' => 4, 'name' => '2 Port channel', 'portchannelCount' => 2, 'iloCount' => 1]
        ];
    }

    public function getPortChannelProfiles()
    {
        return [
            ['id' => 1, 'name' => 'Testprofile 3'],
            ['id' => 2, 'name' => 'Testprofile 4'],
        ];
    }

    public function getIloProfiles()
    {

        return [
            ['id' => 1, 'name' => 'Testprofile 1'],
            ['id' => 2, 'name' => 'Testprofile 2'],
        ];
    }


    public function getIloProfileInterfaces($profileId)
    {

        return [
            ['id' => 1, 'name' => 'Eth1/17'],
            ['id' => 2, 'name' => 'Eth10/10'],
        ];
    }


    public function getIloInterfaceTypes()
    {

        return [
            ['id' => 1, 'name' => 'ILO'],
            ['id' => 2, 'name' => 'MGMT'],
        ];

    }


    public function getPortChannelProfileInterfaces($portChannelProfileId)
    {

        return [
            ['id' => 1, 'name' => 'Eth1/10'],
            ['id' => 2, 'name' => 'Eth10/10'],
        ];
    }


    public function createIloSummary(Request $request)
    {

        $array = [

            'id' => 33,
            'policy_type' => 1,
            'policy_name' => 'LPG_Openstack_MyServer_LACP',
            'profile_config' => [
                'id' => 43,
                'name' => 'aaaaaaaaaaaa'
            ],
            'interface_config' => [
                'id' => 543,
                'name' => 'ccccccccccccc'
            ],
            'interface_type' => [
                'id' => 3,
                'name' => '56456d'
            ],
            'summary' => [
                ['key' => 5, 'value' => [
                    'default' => 543,
                    'all' => [
                        'id' => 543,
                        'name' => 'ccccccccccccc'
                    ]
                ],
                    ['key' => 2, 'value' => [
                        'default' => 543,
                        'all' => [
                            'id' => 543,
                            'name' => 'ccccccccccccc'
                        ]]

                    ]
                ]
            ]
        ];


        return $array;
    }


    public function createPortChannelSummary(Request $request)
    {

        $array = [
            'id' => 22,
            'policy_type' => 2,
            'policy_name' => 'LPG_Openstack_MyServer_LACP',
            'profile_config' => [
                'id' => 43,
                'name' => 'bbbbbbbbbbbb'
            ],
            'interface_config' => [
                'id' => 543,
                'name' => 'dddddddddddddddd'
            ],
            'summary' => [
                ['key' => 8, 'value' => [
                    'default' => 543,
                    'all' => [
                        'id' => 543,
                        'name' => 'ccccccccccccc'
                    ]
                ],
                    ['key' => 9, 'value' => [
                        'default' => 543,
                        'all' => [
                            'id' => 543,
                            'name' => 'ccccccccccccc'
                        ]
                    ]]
                ]
            ]
        ];


        return $array;
    }

    public function storeServerPolicies(Request $request)
    {
        return [
            'status' => 'success',
            'data' => []
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Category|null
     */
    public function show($id)
    {
        $category = Category::find($id);
        if (!$category) {
            return null;
        }
        return $category;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return int
     */
    public function edit($id)
    {
        return $id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
